﻿module presents {
    export interface ITransform {
        Translate?: I3DValue;
        Rotate?: I3DValue;
        Scale?: number;
    }
    export interface I3DValue {
        X: number;
        Y: number;
        Z: number;
    }
    export interface ILayoutManager {
        GetSlides():Slide[];
    }

    module Transform {
        export function rotateZ(deg: number): string {
            return "rotateZ( " + deg + "deg) ";
        }
        export function rotateX(deg: number): string {
            return "rotateX( " + deg + "deg) ";
        }
        export function rotateY(deg: number): string {
            return "rotateY( " + deg + "deg) ";
        }
        export function translate3D(val: I3DValue): string {
            return "translate3d("
                + val.X + "px,"
                + val.Y + "px,"
                + val.Z + "px) ";
        }
        export function perspective(px: number): string {
            return "perspective(" + px + "px) ";
        }
        export function scale(scale: number) {
            return "scale( " + scale + ") ";
        }
    }

    module Utilities {
        export function getText(path: string, callback: (string) => void) {
            var r = new XMLHttpRequest();
            r.open("GET", path, true);
            r.onreadystatechange = () => {
                if (r.readyState != 4 || r.status != 200) return;
                callback(r.responseText);
            };
            r.send();
        }
        export function extend(defaults, options) {
            var extended = {};
            var prop;
            for (prop in defaults) {
                if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
                    extended[prop] = defaults[prop];
                }
            }
            for (prop in options) {
                if (Object.prototype.hasOwnProperty.call(options, prop)) {
                    extended[prop] = options[prop];
                }
            }
            return extended;
        }
    }

    export class SubSectionLayoutManager implements ILayoutManager {
        constructor(private mainSlide: Slide, private _slides: Slide[], private _slideWidth: number, private _containerWidth: number) {

        }
        GetSlides(): Slide[] {
            var slides: Slide[] = [this.mainSlide];

            var count = this._slides.length;
            var left = this.mainSlide.CurrentState.Translate.X - this._containerWidth / 2;
            var w = this._containerWidth / count;
            var scale = w / this._slideWidth;



            for (var i = 0; i < count; i++) {
                var loc = <ITransform>{
                    Translate: {
                        X: left + w / 2 + w * i,
                        Y: this.mainSlide.CurrentState.Translate.Y,
                        Z: this.mainSlide.CurrentState.Translate.Z,
                    },
                    Rotate: { X: 0, Y: 0, Z: 0 },
                    Scale: scale
                };

                this._slides[i].CurrentState = loc;
                this._slides[i].RePosition();

                slides.push(this._slides[i]);
            }
            return slides;
        }
    }

    export class RowLayoutManager implements ILayoutManager{
        private _slides: Slide[] = [];

        constructor(private _location: ITransform, private _slideWidth: number, slides: Slide[]= null) {
            if (slides)
                this.AddSlides(slides);
        }
        AddSlides(slides: Slide[]) {
            slides.forEach((s) => { this.AddSlide(s); });
        }
        AddSlide(slide: Slide) {
            slide.CurrentState = JSON.parse(JSON.stringify(this._location));
            slide.RePosition();
            this._location.Translate.X += this._slideWidth;
            this._slides.push(slide);
        }

        GetSlides(): Slide[] {
            return this._slides;
        }
    }

    export class Location  {
        public CurrentState: ITransform = {
            Translate: { X: 0, Y: 0, Z: 0 },
            Rotate: { X: 0, Y: 0, Z: 0 },
            Scale: 1
        };
        RePosition() {
            var state = this.CurrentState;
            var val = "translate(-50%, -50%) "
                + Transform.translate3D(state.Translate)
                + Transform.rotateX(state.Rotate.X)
                + Transform.rotateY(state.Rotate.Y)
                + Transform.rotateZ(state.Rotate.Z)
                + Transform.scale(state.Scale)
            ;

            this.DisplayElement.style.transform = val;

        }
        DisplayElement: HTMLDivElement;
    }
    export class Slide {
        DisplayElement: HTMLDivElement;
        constructor(url, location: ITransform= {}) {
            this.DisplayElement = document.createElement("div");
            this.DisplayElement.className = "presents-slide";
            this.CurrentState = Utilities.extend(this.CurrentState, location);
            Utilities.getText(url, (res) => {
                this.DisplayElement.innerHTML = res;
                this.LoadedHtml();
            });
            this.RePosition();
            this.Hide();
        }
        LoadedHtml() { }
        public CurrentState: ITransform = {
            Translate: { X: 0, Y: 0, Z: 0 },
            Rotate: { X: 0, Y: 0, Z: 0 },
            Scale: 1
        };
        Show() {
            this.DisplayElement.style.opacity = "1";

        }
        Hide() {
            this.DisplayElement.style.opacity = ".2";
        }
        RePosition() {
            var state = this.CurrentState;
            var val = "translate(-50%, -50%) "
                + Transform.translate3D(state.Translate)
                + Transform.rotateX(state.Rotate.X)
                + Transform.rotateY(state.Rotate.Y)
                + Transform.rotateZ(state.Rotate.Z)
                + Transform.scale(state.Scale)
            ;

            this.DisplayElement.style.transform = val;

        }
    }

    export class CodeSlide extends Slide {
        constructor(url, location: ITransform= {}) {
            super(url, location);
        }
        LoadedHtml() {
            Rainbow.color(this.DisplayElement);
        }
    }

    export class Presentation {
        constructor(elem: HTMLElement, private scaleDefault: number = 1, private zoomBleed: number = .1) {
            this._hostingElement = elem;
            this._cameraElement = document.createElement("div");
            this._cameraElement.className = "camera";
            this._cameraElement.style.transformStyle = "preserve-3d";
            this._hostingElement.appendChild(this._cameraElement);
            this._resetView();
            this._zoomScale = scaleDefault;
        }
        private _zoomScale: number;
        private _hostingElement: HTMLElement;
        private _cameraElement: HTMLElement;
        private _slides: Slide[] = [];
        public SlideIdx = -1;

        private _resetView() {
            this._setCamera({
                Translate: { X: 0, Y: 0, Z: 0 },
                Rotate: { X: 0, Y: 0, Z: 0 },
                Scale: 1
            });
            this.SetView(this.scaleDefault);
            this.SlideIdx = -1;
        }

        private _setCamera(state: ITransform) {
            var val =
                Transform.rotateZ(state.Rotate.Z)
                + Transform.rotateY(state.Rotate.Y)
                + Transform.rotateX(state.Rotate.X)
                + Transform.translate3D(state.Translate)
            ;
            this._cameraElement.style.transform = val;
        }
        private SetView(scale= this.scaleDefault, perspective = 1000) {
            this._zoomScale = scale;
            this._hostingElement.style.transform = Transform.scale(scale)
            + Transform.perspective(perspective);
        }
        private computeWindowScale(displaySlide: Slide) {
            var el = displaySlide.DisplayElement;
            var slideScale = displaySlide.CurrentState.Scale;

            var slide = el.children[0];
            //var displaySlide.CurrentState.Scale < 1
            if (!slide) {
                return 1;
            }
            var tileHeight = slide.clientHeight;
            var tileWidth = slide.clientWidth;
            if (tileWidth < window.innerWidth && tileHeight < window.innerHeight) {
                return 1 / slideScale - this.zoomBleed;
            }
            var hScale = window.innerHeight / slide.clientHeight,
                wScale = window.innerWidth / slide.clientWidth,
                scale = hScale > wScale ? wScale : hScale;

            return (scale - scale * this.zoomBleed) / slideScale;
        }


        AddSlide(slide: Slide) {
            this._slides.push(slide);
            this._cameraElement.appendChild(slide.DisplayElement);
        }

        AddLayout(layout: ILayoutManager) {
            var slides = layout.GetSlides();
            this.AddSlides(slides);
        }

        LookatNextSlide() {
            if (this._slides.length <= this.SlideIdx + 1) {
                this.LookatAll();
                return;
            }
            this.LookatSlide(this.SlideIdx + 1);
        }

        LookatPrevSlide() {
            if (this.SlideIdx == 0) {
                this.LookatAll();
                return;
            }
            this.LookatSlide(this.SlideIdx - 1);
        }
        LookatAll() {
            location.hash = "";
            if (this.SlideIdx >= 0)
                this._slides[this.SlideIdx].Hide();
            this._resetView();
        }
        LookatSlide(idx: number) {
            if (this.SlideIdx >= 0)
                this._slides[this.SlideIdx].Hide();

            this.SlideIdx = idx;

            var currentSlide = this._slides[this.SlideIdx];
            var st = currentSlide.CurrentState;
            var scale = this.computeWindowScale(currentSlide);
            var newState: ITransform = {
                Translate: { X: -st.Translate.X, Y: -st.Translate.Y, Z: -st.Translate.Z },
                Rotate: { X: -st.Rotate.X, Y: -st.Rotate.Y, Z: -st.Rotate.Z },
                // Scale: 1
            };
            this.SetView(scale);
            this._setCamera(newState);
            this._slides[this.SlideIdx].Show();
            location.hash = idx.toString();
        }

        LookatFirstSlide() {
            this.LookatSlide(0);
        }

        LookatLastSlide() {
            this.LookatSlide(this._slides.length - 1);
        }

        AddSlides(slides: Slide[]) {
            slides.forEach(s => { this.AddSlide(s); });
        }

        PullBack() {

            this.SetView(this._zoomScale - .01);
        }

        PushIn() {

            this.SetView(this._zoomScale + .01);
        }
    }

}

