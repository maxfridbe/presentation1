﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var presents;
(function (presents) {
    var Transform;
    (function (Transform) {
        function rotateZ(deg) {
            return "rotateZ( " + deg + "deg) ";
        }
        Transform.rotateZ = rotateZ;
        function rotateX(deg) {
            return "rotateX( " + deg + "deg) ";
        }
        Transform.rotateX = rotateX;
        function rotateY(deg) {
            return "rotateY( " + deg + "deg) ";
        }
        Transform.rotateY = rotateY;
        function translate3D(val) {
            return "translate3d(" + val.X + "px," + val.Y + "px," + val.Z + "px) ";
        }
        Transform.translate3D = translate3D;
        function perspective(px) {
            return "perspective(" + px + "px) ";
        }
        Transform.perspective = perspective;
        function scale(scale) {
            return "scale( " + scale + ") ";
        }
        Transform.scale = scale;
    })(Transform || (Transform = {}));

    var Utilities;
    (function (Utilities) {
        function getText(path, callback) {
            var r = new XMLHttpRequest();
            r.open("GET", path, true);
            r.onreadystatechange = function () {
                if (r.readyState != 4 || r.status != 200)
                    return;
                callback(r.responseText);
            };
            r.send();
        }
        Utilities.getText = getText;
        function extend(defaults, options) {
            var extended = {};
            var prop;
            for (prop in defaults) {
                if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
                    extended[prop] = defaults[prop];
                }
            }
            for (prop in options) {
                if (Object.prototype.hasOwnProperty.call(options, prop)) {
                    extended[prop] = options[prop];
                }
            }
            return extended;
        }
        Utilities.extend = extend;
    })(Utilities || (Utilities = {}));

    var SubSectionLayoutManager = (function () {
        function SubSectionLayoutManager(mainSlide, _slides, _slideWidth, _containerWidth) {
            this.mainSlide = mainSlide;
            this._slides = _slides;
            this._slideWidth = _slideWidth;
            this._containerWidth = _containerWidth;
        }
        SubSectionLayoutManager.prototype.GetSlides = function () {
            var slides = [this.mainSlide];

            var count = this._slides.length;
            var left = this.mainSlide.CurrentState.Translate.X - this._containerWidth / 2;
            var w = this._containerWidth / count;
            var scale = w / this._slideWidth;

            for (var i = 0; i < count; i++) {
                var loc = {
                    Translate: {
                        X: left + w / 2 + w * i,
                        Y: this.mainSlide.CurrentState.Translate.Y,
                        Z: this.mainSlide.CurrentState.Translate.Z
                    },
                    Rotate: { X: 0, Y: 0, Z: 0 },
                    Scale: scale
                };

                this._slides[i].CurrentState = loc;
                this._slides[i].RePosition();

                slides.push(this._slides[i]);
            }
            return slides;
        };
        return SubSectionLayoutManager;
    })();
    presents.SubSectionLayoutManager = SubSectionLayoutManager;

    var RowLayoutManager = (function () {
        function RowLayoutManager(_location, _slideWidth, slides) {
            if (typeof slides === "undefined") { slides = null; }
            this._location = _location;
            this._slideWidth = _slideWidth;
            this._slides = [];
            if (slides)
                this.AddSlides(slides);
        }
        RowLayoutManager.prototype.AddSlides = function (slides) {
            var _this = this;
            slides.forEach(function (s) {
                _this.AddSlide(s);
            });
        };
        RowLayoutManager.prototype.AddSlide = function (slide) {
            slide.CurrentState = JSON.parse(JSON.stringify(this._location));
            slide.RePosition();
            this._location.Translate.X += this._slideWidth;
            this._slides.push(slide);
        };

        RowLayoutManager.prototype.GetSlides = function () {
            return this._slides;
        };
        return RowLayoutManager;
    })();
    presents.RowLayoutManager = RowLayoutManager;

    var Location = (function () {
        function Location() {
            this.CurrentState = {
                Translate: { X: 0, Y: 0, Z: 0 },
                Rotate: { X: 0, Y: 0, Z: 0 },
                Scale: 1
            };
        }
        Location.prototype.RePosition = function () {
            var state = this.CurrentState;
            var val = "translate(-50%, -50%) " + Transform.translate3D(state.Translate) + Transform.rotateX(state.Rotate.X) + Transform.rotateY(state.Rotate.Y) + Transform.rotateZ(state.Rotate.Z) + Transform.scale(state.Scale);

            this.DisplayElement.style.transform = val;
        };
        return Location;
    })();
    presents.Location = Location;
    var Slide = (function () {
        function Slide(url, location) {
            if (typeof location === "undefined") { location = {}; }
            var _this = this;
            this.CurrentState = {
                Translate: { X: 0, Y: 0, Z: 0 },
                Rotate: { X: 0, Y: 0, Z: 0 },
                Scale: 1
            };
            this.DisplayElement = document.createElement("div");
            this.DisplayElement.className = "presents-slide";
            this.CurrentState = Utilities.extend(this.CurrentState, location);
            Utilities.getText(url, function (res) {
                _this.DisplayElement.innerHTML = res;
                _this.LoadedHtml();
            });
            this.RePosition();
            this.Hide();
        }
        Slide.prototype.LoadedHtml = function () {
        };

        Slide.prototype.Show = function () {
            this.DisplayElement.style.opacity = "1";
        };
        Slide.prototype.Hide = function () {
            this.DisplayElement.style.opacity = ".2";
        };
        Slide.prototype.RePosition = function () {
            var state = this.CurrentState;
            var val = "translate(-50%, -50%) " + Transform.translate3D(state.Translate) + Transform.rotateX(state.Rotate.X) + Transform.rotateY(state.Rotate.Y) + Transform.rotateZ(state.Rotate.Z) + Transform.scale(state.Scale);

            this.DisplayElement.style.transform = val;
        };
        return Slide;
    })();
    presents.Slide = Slide;

    var CodeSlide = (function (_super) {
        __extends(CodeSlide, _super);
        function CodeSlide(url, location) {
            if (typeof location === "undefined") { location = {}; }
            _super.call(this, url, location);
        }
        CodeSlide.prototype.LoadedHtml = function () {
            Rainbow.color(this.DisplayElement);
        };
        return CodeSlide;
    })(Slide);
    presents.CodeSlide = CodeSlide;

    var Presentation = (function () {
        function Presentation(elem, scaleDefault, zoomBleed) {
            if (typeof scaleDefault === "undefined") { scaleDefault = 1; }
            if (typeof zoomBleed === "undefined") { zoomBleed = .1; }
            this.scaleDefault = scaleDefault;
            this.zoomBleed = zoomBleed;
            this._slides = [];
            this.SlideIdx = -1;
            this._hostingElement = elem;
            this._cameraElement = document.createElement("div");
            this._cameraElement.className = "camera";
            this._cameraElement.style.transformStyle = "preserve-3d";
            this._hostingElement.appendChild(this._cameraElement);
            this._resetView();
            this._zoomScale = scaleDefault;
        }
        Presentation.prototype._resetView = function () {
            this._setCamera({
                Translate: { X: 0, Y: 0, Z: 0 },
                Rotate: { X: 0, Y: 0, Z: 0 },
                Scale: 1
            });
            this.SetView(this.scaleDefault);
            this.SlideIdx = -1;
        };

        Presentation.prototype._setCamera = function (state) {
            var val = Transform.rotateZ(state.Rotate.Z) + Transform.rotateY(state.Rotate.Y) + Transform.rotateX(state.Rotate.X) + Transform.translate3D(state.Translate);
            this._cameraElement.style.transform = val;
        };
        Presentation.prototype.SetView = function (scale, perspective) {
            if (typeof scale === "undefined") { scale = this.scaleDefault; }
            if (typeof perspective === "undefined") { perspective = 1000; }
            this._zoomScale = scale;
            this._hostingElement.style.transform = Transform.scale(scale) + Transform.perspective(perspective);
        };
        Presentation.prototype.computeWindowScale = function (displaySlide) {
            var el = displaySlide.DisplayElement;
            var slideScale = displaySlide.CurrentState.Scale;

            var slide = el.children[0];

            //var displaySlide.CurrentState.Scale < 1
            if (!slide) {
                return 1;
            }
            var tileHeight = slide.clientHeight;
            var tileWidth = slide.clientWidth;
            if (tileWidth < window.innerWidth && tileHeight < window.innerHeight) {
                return 1 / slideScale - this.zoomBleed;
            }
            var hScale = window.innerHeight / slide.clientHeight, wScale = window.innerWidth / slide.clientWidth, scale = hScale > wScale ? wScale : hScale;

            return (scale - scale * this.zoomBleed) / slideScale;
        };

        Presentation.prototype.AddSlide = function (slide) {
            this._slides.push(slide);
            this._cameraElement.appendChild(slide.DisplayElement);
        };

        Presentation.prototype.AddLayout = function (layout) {
            var slides = layout.GetSlides();
            this.AddSlides(slides);
        };

        Presentation.prototype.LookatNextSlide = function () {
            if (this._slides.length <= this.SlideIdx + 1) {
                this.LookatAll();
                return;
            }
            this.LookatSlide(this.SlideIdx + 1);
        };

        Presentation.prototype.LookatPrevSlide = function () {
            if (this.SlideIdx == 0) {
                this.LookatAll();
                return;
            }
            this.LookatSlide(this.SlideIdx - 1);
        };
        Presentation.prototype.LookatAll = function () {
            location.hash = "";
            if (this.SlideIdx >= 0)
                this._slides[this.SlideIdx].Hide();
            this._resetView();
        };
        Presentation.prototype.LookatSlide = function (idx) {
            if (this.SlideIdx >= 0)
                this._slides[this.SlideIdx].Hide();

            this.SlideIdx = idx;

            var currentSlide = this._slides[this.SlideIdx];
            var st = currentSlide.CurrentState;
            var scale = this.computeWindowScale(currentSlide);
            var newState = {
                Translate: { X: -st.Translate.X, Y: -st.Translate.Y, Z: -st.Translate.Z },
                Rotate: { X: -st.Rotate.X, Y: -st.Rotate.Y, Z: -st.Rotate.Z }
            };
            this.SetView(scale);
            this._setCamera(newState);
            this._slides[this.SlideIdx].Show();
            location.hash = idx.toString();
        };

        Presentation.prototype.LookatFirstSlide = function () {
            this.LookatSlide(0);
        };

        Presentation.prototype.LookatLastSlide = function () {
            this.LookatSlide(this._slides.length - 1);
        };

        Presentation.prototype.AddSlides = function (slides) {
            var _this = this;
            slides.forEach(function (s) {
                _this.AddSlide(s);
            });
        };

        Presentation.prototype.PullBack = function () {
            this.SetView(this._zoomScale - .01);
        };

        Presentation.prototype.PushIn = function () {
            this.SetView(this._zoomScale + .01);
        };
        return Presentation;
    })();
    presents.Presentation = Presentation;
})(presents || (presents = {}));
//# sourceMappingURL=presen.js.map
