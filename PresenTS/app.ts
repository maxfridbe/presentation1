﻿declare var Rainbow;
window.onload = () => {
    var el = document.getElementById('content');
    var presentation = new presents.Presentation(el, .3, .2);

    window.onhashchange = (ev: Event) => {
        var num = window.location.hash.substr(1);
        try {
            var idx = parseInt(num);
            if (!isNaN(idx) && idx !== presentation.SlideIdx) {
                console.log("switing to slide " + idx);
                presentation.LookatSlide(idx);
            }
        } catch (e) {
            console.error(e);
        }
    };

    window.onkeydown = (ev: KeyboardEvent) => {
        if (ev.keyCode == 27) {//esc = 27
            presentation.LookatAll();
        }
        if (ev.keyCode == 39) {//right = 39
            if (presentation.SlideIdx >= 0) {
                presentation.LookatNextSlide();
            } else {
                presentation.LookatFirstSlide();
            }
            ev.preventDefault();
        }
        if (ev.keyCode == 37) {//left = 37
            if (presentation.SlideIdx < 0) {
                presentation.LookatLastSlide();
            } else {
                presentation.LookatPrevSlide();
            }

            ev.preventDefault();
        }
        if (ev.keyCode == 189) {//-
            presentation.PullBack();
            ev.preventDefault();
        }
        if (ev.keyCode == 187) {//+
            presentation.PushIn();
            ev.preventDefault();
        }
    };

    var top = -800;
    var left = -800;
    var firstRowHeight = 600;
    var secondRowHeight = 900;
    var exampleTileWidth = 1000;

    presentation.AddSlide(new presents.Slide("slides/1-intro.htm", { Translate: { X: left, Y: top, Z: 0 } }));
    presentation.AddLayout(
        new presents.SubSectionLayoutManager(
            new presents.Slide("slides/lesson/2-lessons.htm", { Translate: { X: 0, Y: top + firstRowHeight + 300, Z: 0 } }),
            [
                new presents.Slide("slides/lesson/what.htm"),
                new presents.CodeSlide("slides/lesson/2-1.htm"),
                new presents.CodeSlide("slides/lesson/2-1-1.htm"),
                new presents.CodeSlide("slides/lesson/2-1-2.htm"),
                new presents.CodeSlide("slides/lesson/2-1-3.htm"),
                new presents.CodeSlide("slides/lesson/2-2.htm"),
                new presents.CodeSlide("slides/lesson/2-2-1.htm"),
                new presents.CodeSlide("slides/lesson/2-2-2.htm"),
                new presents.Slide("slides/lesson/2-3.htm"),
                new presents.Slide("slides/lesson/2-3-1.htm"),
                new presents.Slide("slides/lesson/no.htm"),
                new presents.Slide("slides/lesson/2-fin-1.htm")
            ],
            1000,
            2500
            ));
    presentation.AddSlide(new presents.Slide("slides/software/software.htm", {
        Translate: { X: left + exampleTileWidth, Y: top, Z: 0 },
        Rotate: { X: 0, Y: 0, Z: 0 },
        Scale: 1
    }));
    presentation.AddSlide(new presents.Slide("slides/software/psycho.htm", {
        Translate: { X: left + exampleTileWidth * 2, Y: top, Z: 0 },
        Rotate: { X: 0, Y: 0, Z: 90 },
        Scale: .2
    }));
    presentation.AddLayout(
        new presents.SubSectionLayoutManager(new presents.Slide("slides/software/scaling/scaling.htm", {
            Translate: { X: left + exampleTileWidth * 3, Y: top, Z: 0 },
            Rotate: { X: 0, Y: 0, Z: 0 },
            Scale: 1
        }),
            [
                new presents.Slide("slides/software/scaling/why.htm"),
                new presents.Slide("slides/software/scaling/how.htm"),
            ],
            1000,
            1000));
    presentation.AddLayout(
        new presents.SubSectionLayoutManager(new presents.Slide("slides/software/abstraction/abstraction.htm", {
            Translate: { X: left + exampleTileWidth * 4, Y: top, Z: 0 },
            Rotate: { X: 0, Y: 0, Z: 0 },
            Scale: 1
        }),
            [
                new presents.Slide("slides/software/abstraction/why.htm"),
                new presents.Slide("slides/software/abstraction/how.htm"),
            ],
            1000,
            1000));
    presentation.AddLayout(
        new presents.SubSectionLayoutManager(new presents.Slide("slides/software/encapsulation/encapsulation.htm", {
            Translate: { X: left + exampleTileWidth * 5, Y: top, Z: 0 },
            Rotate: { X: 0, Y: 0, Z: 0 },
            Scale: 1
        }),
            [
                new presents.Slide("slides/software/encapsulation/why.htm"),
                new presents.Slide("slides/software/encapsulation/how.htm"),
            ],
            1000,
            1000));

    var evolutionH = top + firstRowHeight + 400 + secondRowHeight;
    presentation.AddSlide(new presents.Slide("slides/evolution/3-evolution.htm", { Translate: { X: 0, Y: top + firstRowHeight + 300 + secondRowHeight, Z: 0 }, }));
    
    presentation.AddLayout(new presents.SubSectionLayoutManager(
        new presents.Slide("slides/evolution/past/3-1-past.htm", { Translate: { X: left + exampleTileWidth * 0, Y: evolutionH, Z: 0 } }),
        [
            new presents.Slide("slides/evolution/past/Desktop.htm"),
            new presents.Slide("slides/evolution/past/Client.htm"),
        ],
        1000,
        1000
        ));
    presentation.AddLayout(new presents.SubSectionLayoutManager(
        new presents.Slide("slides/evolution/past/scaling.htm", { Translate: { X: left + exampleTileWidth * 1, Y: evolutionH, Z: 0 } }),
        [
            new presents.CodeSlide("slides/evolution/past/encapsulation.htm"),
            new presents.Slide("slides/evolution/past/abstraction.htm"),
        ],
        1000,
        1000
        ));

    presentation.AddLayout(new presents.SubSectionLayoutManager(
        new presents.Slide("slides/evolution/present/3-2-present.htm", { Translate: { X: left + exampleTileWidth * 2, Y: evolutionH, Z: 0 } }),
        [
            new presents.Slide("slides/evolution/present/Desktop.htm"),
            new presents.Slide("slides/evolution/present/Client.htm"),
        ],
        1000,
        1000
        ));
    presentation.AddLayout(new presents.SubSectionLayoutManager(
        new presents.Slide("slides/evolution/present/scaling.htm", { Translate: { X: left + exampleTileWidth * 3, Y: evolutionH, Z: 0 } }),
        [
            new presents.Slide("slides/evolution/present/encapsulation.htm"),
            new presents.Slide("slides/evolution/present/abstraction.htm"),
        ],
        1000,
        1000
        ));
    


    presentation.AddLayout(new presents.SubSectionLayoutManager(
        new presents.Slide("slides/evolution/future/3-3-future.htm", { Translate: { X: left + exampleTileWidth * 4, Y: top + firstRowHeight + 400 + secondRowHeight, Z: 0 } }),
        [
            new presents.Slide("slides/evolution/future/Desktop.htm"),
            new presents.Slide("slides/evolution/future/Client.htm"),
        ],
        1000,
        1000
        ));
    presentation.AddLayout(new presents.SubSectionLayoutManager(
        new presents.Slide("slides/evolution/future/scaling.htm", { Translate: { X: left + exampleTileWidth * 5, Y: evolutionH, Z: 0 } }),
        [
            new presents.CodeSlide("slides/evolution/future/encapsulation.htm"),
            new presents.CodeSlide("slides/evolution/future/abstraction.htm"),
        ],
        1000,
        1000
        ));
    presentation.AddSlide(new presents.Slide("slides/evolution/futureisnow.htm", { Translate: { X: left + exampleTileWidth * 7, Y: evolutionH, Z: 0 } }));
    presentation.AddSlide(new presents.Slide("slides/evolution/web.htm", { Translate: { X: left + exampleTileWidth * 8, Y: evolutionH, Z: 0 } }));

    presentation.AddSlide(new presents.Slide("slides/4-thanks.htm", { Translate: { X: left + exampleTileWidth, Y: top, Z: -100 }, Scale: .03 }));


    window.onhashchange(null);

};
